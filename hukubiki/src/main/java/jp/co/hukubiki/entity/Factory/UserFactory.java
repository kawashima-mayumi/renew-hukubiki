package jp.co.hukubiki.entity.Factory;

import org.springframework.stereotype.Component;

import jp.co.hukubiki.dto.UserDto;
import jp.co.hukubiki.entity.User;

@Component
public class UserFactory {
	public User regist(UserDto user) {
		return new User(
				null,
				user.getName(),
				user.getLogin_id(),
				user.getPassword()
				);

	}
}
