package jp.co.hukubiki.mapper;

import org.springframework.stereotype.Component;

import jp.co.hukubiki.entity.User;

@Component
public interface UserMapper {
	void regist(User user);

	User getUser(String login_id, String password);
}
