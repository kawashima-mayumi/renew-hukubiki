package jp.co.hukubiki.dto.dtoFactory;

import org.springframework.stereotype.Component;

import jp.co.hukubiki.dto.UserDto;
import jp.co.hukubiki.form.SignupForm;
import jp.co.hukubiki.utils.CipherUtil;

@Component
public class UserDtoFactory {
	public UserDto create (SignupForm form) {
		return new UserDto(
				null,
				form.getName(),
				form.getLogin_id(),
				CipherUtil.encrypt(form.getPassword())
				);

	}
}
