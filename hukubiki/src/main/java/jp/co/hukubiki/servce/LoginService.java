package jp.co.hukubiki.servce;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.hukubiki.dto.UserDto;
import jp.co.hukubiki.dto.dtoFactory.UserDtoFactory;
import jp.co.hukubiki.entity.User;
import jp.co.hukubiki.mapper.UserMapper;
import jp.co.hukubiki.utils.CipherUtil;

@Service
public class LoginService {
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserDtoFactory userDtoFactory;

	public UserDto login(String login_id, String password) {
		User user = userMapper.getUser(login_id, CipherUtil.encrypt(password));
//		return userDtoFactory.create(user);
	}

}
