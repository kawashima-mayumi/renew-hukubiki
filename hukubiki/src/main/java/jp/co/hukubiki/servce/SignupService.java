package jp.co.hukubiki.servce;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.hukubiki.dto.UserDto;
import jp.co.hukubiki.entity.Factory.UserFactory;
import jp.co.hukubiki.mapper.UserMapper;

@Service
public class SignupService {
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserFactory userFactory;


	public void registUser(UserDto user) {
		userMapper.regist(userFactory.regist(user));
	}

}
