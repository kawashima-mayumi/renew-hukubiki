package jp.co.hukubiki.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.hukubiki.dto.UserDto;
import jp.co.hukubiki.form.LoginForm;
import jp.co.hukubiki.servce.LoginService;

@Controller
public class LoginController {
	@Autowired
	private LoginService loginService;
	 @RequestMapping(value = "/login", method = RequestMethod.POST)
	    public String doPost(@ModelAttribute LoginForm loginForm, Model model, BindingResult result) {
		 if(result.hasErrors()){
	        	return "signup";
		 }
		 UserDto user = loginService.login(loginForm.getLogin_id(), loginForm.getPassword());

	 }
}
