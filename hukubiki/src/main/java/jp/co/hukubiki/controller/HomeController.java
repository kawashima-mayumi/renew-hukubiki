package jp.co.hukubiki.controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.hukubiki.dto.UserDto;
import jp.co.hukubiki.form.LoginForm;


@Controller
public class HomeController extends HttpServlet {
	@Autowired
	private HttpServletRequest request;

	 @RequestMapping(value = "/home", method = RequestMethod.GET)
	    public String showMessage(@ModelAttribute LoginForm loginForm, Model model) {
		 UserDto user = (UserDto) request.getSession().getAttribute("loginUser");
		 	model.addAttribute("loginForm", loginForm);
	        model.addAttribute("message", "福引をして装備を増やそう");
	        model.addAttribute("loginUser", user);
	        return "/home";
	    }
}
