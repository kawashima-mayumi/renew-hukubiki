package jp.co.hukubiki.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.hukubiki.dto.dtoFactory.UserDtoFactory;
import jp.co.hukubiki.form.SignupForm;
import jp.co.hukubiki.servce.SignupService;

@Controller
public class SignupController {
	@Autowired
	private SignupService signupService;
	@Autowired
	private UserDtoFactory userDtoFactory;

	 @RequestMapping(value = "/signup", method = RequestMethod.GET)
	    public String doGet(@ModelAttribute SignupForm signupForm,  Model model) {
	        model.addAttribute("signupForm", signupForm);
	        return "/signup";
	    }

	 @RequestMapping(value = "/signup", method = RequestMethod.POST)
	    public String doPost(@ModelAttribute SignupForm signupForm,  Model model,  BindingResult result) {
	        if(result.hasErrors()) {
	        	return "signup";
	        }try {
				signupService.registUser(userDtoFactory.create(signupForm));
			}catch(Exception e) {
				return "signup";
			}

	        return "redirect:home";
	    }
}
